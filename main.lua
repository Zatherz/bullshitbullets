crystal = require("crystal")
--Copyright 2016 Dominik "Zatherz" Banaszak

--Licensed under the Apache License, Version 2.0 (the "License");
--you may not use this file except in compliance with the License.
--You may obtain a copy of the License at

--    http://www.apache.org/licenses/LICENSE-2.0

--Unless required by applicable law or agreed to in writing, software
--distributed under the License is distributed on an "AS IS" BASIS,
--WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--See the License for the specific language governing permissions and
--limitations under the License.

local window = am.window({
	title = "Bullshit Bullets",
	width = 400,
	height = 300,
	mode = "fullscreen",
	lock_pointer = true
})
window.scene = am.group()
local sprites = {
	player = [[
		..b..
		.bBb.
		bBBBb
		.bBb.
		..b..
	]],
	bullets = {
		["bullet"] = [[
			.ww.
			wWWw
			wWWw
			.ww.
		]],
		["explosivebullet"] = [[
			.rr.
			rWWr
			rWWr
			.rr.
		]],
		["flaretrail"] = [[
			.RR.
			RRRR
			RRRR
			.RR.
		]],
		["flarebullet"] = [[
			.yy.
			yYYy
			yYYy
			.yy.
		]],
		["horizontalbullet"] = [[
			ccc
			cCc
			cCc
			cCc
			cCc
			cCc
			cCc
			cCc
			cCc
			cCc
			cCc
			ccc
		]],
		["platform"] = [[
			wwwwwwwwwwwwwwwwwwwwwwwwwwwww
			wWWWWWWWWWWWWWWWWWWWWWWWWWWWw
			wwwwwwwwwwwwwwwwwwwwwwwwwwwww
		]],
		["superfastplatform"] = [[
			rrrrrrrrrrrrrrrrrrrrrrrrrrrrr
			rRRRRRRRRRRRRRRRRRRRRRRRRRRRr
			rRRRRRRRRRRRRRRRRRRRRRRRRRRRr
			rRRRRRRRRRRRRRRRRRRRRRRRRRRRr
			rRRRRRRRRRRRRRRRRRRRRRRRRRRRr
			rrrrrrrrrrrrrrrrrrrrrrrrrrrrr
		]],
		["superfastplatformindicator"] = [[
			RRRRRR
			RRRRRR
			RRRRRR
			RRRRRR
			RRRRRR
			RRRRRR
		]]
	}
}

local state = "prestart"
local timers = {}
local bullettypes = 1
local bullets = {}
local objects = {}
local difficulty = {
	current = 0,
	last = 0,
	difficulties = {
		[1] = {type = "explosivebullet", pos = "h"},
		[2] = {type = "flarebullet", pos = "h"},
		[3] = {type = "horizontalbullet", pos = "v"},
		[4] = {type = "platform", pos = "v"},
		[5] = {type = "platform", pos = "h"},
		[6] = {type = "verticalexplosivebullet", pos = "v"},
		[7] = {type = "flarebullet", pos = "v"},
		[8] = {type = "superfastplatformindicator", pos = "v"}
	},
	active = {
		{type = "bullet", pos = "h"}
	}
}

timers = {
	reset = function()
		timers.timers.bulletspawn = {max = 0.15, current = 0.15}
	end,
	timers = {
		bulletspawn = {}
	}
}
timers.reset()

bullets = {
	["bullet"] = {
		speed = 300,
		sprite = sprites.bullets["bullet"],
		func = function(node)
			node.position = node.position + node.properties.direction * am.delta_time * node.properties.speed
			if node.position.x > window.width/2 + 1 or node.position.x < -window.width/2 - 1 or
					node.position.y > window.height/2 + 1 or node.position.y < -window.height/2 - 1 then
				window.scene:remove(node)
			end
		end
	},
	["explosivebullet"] = {
		speed = 300,
		sprite = sprites.bullets["explosivebullet"],
		func = function(node)
			node.properties.explodey = node.properties.explodey or math.random(0, window.height/2)
			bullets["bullet"].func(node) -- inherit "bullet"
			if node.position.y > node.properties.explodey - 5 and node.position.y < node.properties.explodey + 5 then
				window.scene:append(objects.bullet("bullet", vec2(1, 0), node.position.x, node.position.y, 0.3))
				window.scene:append(objects.bullet("bullet", vec2(-1, 0), node.position.x, node.position.y, 0.3))
				window.scene:append(objects.bullet("bullet", vec2(1, -2), node.position.x, node.position.y, 0.3))
				window.scene:append(objects.bullet("bullet", vec2(-1, -2), node.position.x, node.position.y, 0.3))
				window.scene:remove(node)
			end
		end
	},
	["verticalexplosivebullet"] = {
		speed = 300,
		sprite = sprites.bullets["explosivebullet"],
		func = function(node)
			node.properties.explodex = node.properties.explodex or math.random(0, window.width/2)
			bullets["bullet"].func(node) -- inherit "bullet"
			if node.position.x > node.properties.explodex - 5 and node.position.x < node.properties.explodex + 5 then
				window.scene:append(objects.bullet("bullet", vec2(1, 0), node.position.x, node.position.y, 0.15))
				window.scene:append(objects.bullet("bullet", vec2(-1, 0), node.position.x, node.position.y, 0.15))
				window.scene:append(objects.bullet("bullet", vec2(1, -2), node.position.x, node.position.y, 0.15))
				window.scene:append(objects.bullet("bullet", vec2(-1, -2), node.position.x, node.position.y, 0.15))
				window.scene:remove(node)
			end
		end
	},
	["flaretrail"] = { -- TECHNICAL
		speed = 0,
		sprite = sprites.bullets["flaretrail"],
		func = function(node)
			node.color = vec4(node.color.r, node.color.g, node.color.b, node.color.a - am.delta_time * 0.8)
			if node.color.a <= 0 then
				window.scene:remove(node)
			end
		end
	},
	["flarebullet"] = {
		speed = 200,
		sprite = sprites.bullets["flarebullet"],
		func = function(node)
			bullets["bullet"].func(node) -- inherit "bullet"
			local placetrail = true
			window.scene:append(objects.bullet("flaretrail", nil, node.position.x, node.position.y))
		end
	},
	["horizontalbullet"] = {
		speed = 300,
		sprite = sprites.bullets["horizontalbullet"],
		func = function(node)
			bullets["bullet"].func(node) -- inherit "bullet"
		end
	},
	["platform"] = {
		speed = 300,
		sprite = sprites.bullets["platform"],
		func = function(node)
			bullets["bullet"].func(node) -- inherit "bullet"
		end
	},
	["superfastplatform"] = {
		speed = 3000,
		sprite = sprites.bullets["superfastplatform"],
		func = function(node)
			bullets["bullet"].func(node) -- inherit "bullet"
		end
	},
	["superfastplatformindicator"] = {
		speed = 0,
		sprite = sprites.bullets["superfastplatformindicator"],
		func = function(node)
			node.properties.life = node.properties.life or 0
			node.properties.life = node.properties.life + am.delta_time
			if node.properties.life > 3 then
				window.scene:append(objects.bullet("superfastplatform", vec2(1, 0), node.position.x, node.position.y))
				window.scene:remove(node)
			end
		end
	}
}

objects = {
	timealive = function()
		return crystal.text("Time survived: ", vec2(-window.width/2, window.height/2), vec4(1), vec2(1), 0, {type = "timealive"})
	end,
	highscore = function()
		return crystal.text("High score: ", vec2(-window.width/2, window.height/2), vec4(1), vec2(1), 0, {type = "highscore"})
	end,
	player = function()
		return crystal.sprite(sprites.player, vec2(0, 0), vec4(1), vec2(2), 0, {type = "player", speed = 150}):action(function(node)
			if state == "playing" then
				if crystal.keys_down(window, "w", "up") then
					node.position = math.clamp(vec2(node.position.x, node.position.y + am.delta_time * node.properties.speed), vec2(-window.width/2, -window.height/2), vec2(window.width/2, window.height/2))
				end
				if crystal.keys_down(window, "s", "down") then
					node.position = math.clamp(vec2(node.position.x, node.position.y - am.delta_time * node.properties.speed), vec2(-window.width/2, -window.height/2), vec2(window.width/2, window.height/2))
				end
				if crystal.keys_down(window, "a", "left") then
					node.position = math.clamp(vec2(node.position.x - am.delta_time * node.properties.speed, node.position.y), vec2(-window.width/2, -window.height/2), vec2(window.width/2, window.height/2))
				end
				if crystal.keys_down(window, "d", "right") then
					node.position = math.clamp(vec2(node.position.x + am.delta_time * node.properties.speed, node.position.y), vec2(-window.width/2, -window.height/2), vec2(window.width/2, window.height/2))
				end
				for index, bullet in ipairs(crystal.get_all_objects_by_property(window.scene, "type", "bullet")) do
					if node:aabb_collision(bullet) then
						state = "gameover"
					end
				end
			end
		end)
	end,
	bullet = function(type, direction, x, y, speedmult)
		type = type or "bullet"
		direction = direction or vec2(0, -1)
		speedmult = speedmult or 1
		assert(bullets[type] and bullets[type].func and bullets[type].speed and bullets[type].sprite, "Bullet type doesn't exist or is incomplete")
		return crystal.sprite(bullets[type].sprite, vec2(x, y), vec4(1), vec2(2), 0, {type = "bullet", bullet = type, speed = bullets[type].speed * speedmult, direction = direction}):action(function(node)
			if state == "playing" then
				bullets[type].func(node)
			end
		end)
	end
}

local alive = 0
local highscorestate = am.load_state("bullshitbullets") or 0

function refreshhighscore()
	return am.load_state("bullshitbullets") or 0
end

function setup(scene)
	scene:append(objects.player())
	scene:append(objects.timealive())
	scene:append(objects.highscore())
end

function reset()
	timers.reset()
end

function nicealive()
	local format = "%.02fs"

	if alive > highscorestate then
		format = format .. " (high score!)"
	end
	return string.format(format, alive)
end

function nicehighscorestate()
	return string.format("%.02fs", highscorestate)
end

function close()
	if alive > highscorestate then
		am.save_state("bullshitbullets", alive)
	end
	window:close()
end

window.scene:action(function(node)
	if window:key_pressed("m") then
		window.lock_pointer = not window.lock_pointer
	end
	if window:key_pressed("f") then
		if window.mode == "windowed" then
			window.mode = "fullscreen"
		else
			window.mode = "windowed"
		end
	end
	if state == "prestart" then
		node:append(crystal.text("BULLSHIT BULLETS\nPress Space to start or Q to exit.\n\nIngame controls:\nArrow keys or WASD to move\nEsc or P to pause\nR to reset highscore\nEsc+Q or P+Q to quit\nF to toggle fullscreen\nM to toggle pointer lock", vec2(0, 0), vec4(1), vec2(1), 0, {type = "prestartmsg"}))
		state = "prestart_waiting"
	elseif state == "prestart_waiting" then
		if window:key_down("space") then
			node:remove(crystal.get_object_by_property(node, "type", "prestartmsg"))
			setup(window.scene)
			state = "playing"
		end
		if window:key_down("q") then
			close()
		end
	elseif state == "playing" then -- START INGAME LOOP
		if crystal.keys_pressed(window, "escape", "p") then
			state = "pause"
		end
		if window:key_down("r") then
			state = "reset_highscore"
		end
		difficulty.current = math.floor(alive / 10)
		alive = alive + am.delta_time
		if difficulty.current ~= difficulty.last then
			difficulty.last = difficulty.current
			table.clear(difficulty.active)
			table.insert(difficulty.active, {type = "bullet", pos = "h"})
			for i = 1, difficulty.current do
				table.insert(difficulty.active, difficulty.difficulties[math.random(1, #difficulty.difficulties)])
			end
		end
		-- align and update time alive & high score text
		local timealiveobj = crystal.get_object_by_property(node, "type", "timealive")
		timealiveobj.text = "Time survived: " .. nicealive()
		timealiveobj.position = vec2(-window.width/2 + timealiveobj.width/2, window.height/2 - timealiveobj.height / 4)

		highscoreobj = crystal.get_object_by_property(node, "type", "highscore")
		highscoreobj.text = "High score: " .. nicehighscorestate()
		highscoreobj.position = vec2(-window.width/2 + highscoreobj.width/2, window.height/2 - timealiveobj.height)

		if timers.timers.bulletspawn.current <= 0 then
			timers.timers.bulletspawn.current = timers.timers.bulletspawn.max
			local x = math.random(-window.width/2, window.width/2)
			local y = math.random(-window.height/2, window.height/2)
			local bullet = math.random(1, #difficulty.active)
			if difficulty.active[bullet].pos == "h" then
				window.scene:append(objects.bullet(difficulty.active[bullet].type, nil, x, window.height/2))
			elseif difficulty.active[bullet].pos == "v" then
				window.scene:append(objects.bullet(difficulty.active[bullet].type, vec2(1, 0), -window.width/2, y))
			end
		else
			timers.timers.bulletspawn.current = timers.timers.bulletspawn.current - am.delta_time
		end
	elseif state == "gameover" then -- END INGAME LOOP
		node:remove_all()
		node:append(crystal.text("Game over! You've survived " .. nicealive() .. ".\nPress Space to restart or Q to exit.", vec2(0, 0), vec4(1), vec2(1), 0, {type = "gameovermsg"}))
		state = "gameover_waiting"
	elseif state == "gameover_waiting" then
		if window:key_down("space") then
			node:remove(crystal.get_object_by_property(node, "type", "gameovermsg"))
			if alive > highscorestate then
				am.save_state("bullshitbullets", alive)
				highscorestate = refreshhighscore()
			end
			alive = 0
			bullettypes = 1
			setup(window.scene)
			state = "playing"
		end
		if window:key_down("q") then
			close()
		end
	elseif state == "reset_highscore" then
		node:append(crystal.text("Reset highscore?\nY for yes\nN or Esc for no", vec2(0, 0), vec4(1), vec2(1), 0, {type = "resethighscoremsg"}))
		state = "reset_highscore_waiting"
	elseif state == "reset_highscore_waiting" then
		if window:key_down("y") then
			am.save_state("bullshitbullets", 0)
			node:remove(crystal.get_object_by_property(node, "type", "resethighscoremsg"))
			highscorestate = refreshhighscore()
			state = "playing"
		elseif crystal.keys_down(window, "n", "escape") then
			node:remove(crystal.get_object_by_property(node, "type", "resethighscoremsg"))
			state = "playing"
		end
	elseif state == "pause" then
		node:append(crystal.text("Paused\nQ to exit\nEsc or P to resume", vec2(0, 0), vec4(1), vec2(1), 0, {type = "pausemsg"}))
		state = "pause_waiting"
	elseif state == "pause_waiting" then
		if crystal.keys_pressed(window, "escape", "p") then
			node:remove(crystal.get_object_by_property(node, "type", "pausemsg"))
			state = "playing"
		end
		if window:key_down("q") then
			close()
		end
	end
end)
