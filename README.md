# Bullshit Bullets (2.0)
Bullshit Bullets is a very simple game where you have to dodge bullets. It was made to practice dodging.  
Version 2.0 is a huge update to the game, with big refactoring and support for different types of bullets + real difficulty.

# Play online
[You can play this game online now!](http://zatherz.gitlab.io/bullshitbullets)

# Dependencies
* Amulet v1.0.9 (http://amulet.xyz)
* Crystal v0.01 (http://gitlab.com/zatherz/crystal)

# Licenses
See `LICENSES.txt`

# Download
[Windows](https://gitlab.com/Zatherz/bullshitbullets/raw/master/export/bullshitbullets-2.0-windows.zip) (32-bit)  
[Linux](https://gitlab.com/Zatherz/bullshitbullets/raw/master/export/bullshitbullets-2.0-linux.zip) (64- and 32-bit)  
[Mac](https://gitlab.com/Zatherz/bullshitbullets/raw/master/export/bullshitbullets-2.0-mac.zip) (64-bit)  
[HTML5](https://gitlab.com/Zatherz/bullshitbullets/raw/master/export/bullshitbullets-2.0-html.zip)  (Emscripten)
