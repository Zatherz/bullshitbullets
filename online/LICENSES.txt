This program is licensed under the Apache 2 license, available at http://www.apache.org/licenses/LICENSE-2.0
This program uses components licensed under the following licenses:
* Crystal (http://gitlab.com/zatherz/crystal): http://www.apache.org/licenses/LICENSE-2.0
* Amulet (http://amulet.xyz): MIT, included as "amulet\_license.txt" in exported packages
